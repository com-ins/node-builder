#!/usr/bin/env bash

#colors
color_success="\\033[1;32m"
color_failure="\\033[1;31m"
color_normal="\\033[0;39m"

tmp=/tmp_src

check_status()
{
if  [[ $? != 0 ]];then
    echo -e "$color_failure""FAILURE""$color_normal"
    exit 1
fi
echo -e "$color_success""OK""$color_normal"
}

echo -e "$color_success""Start build process""$color_normal"

echo -e "$color_success""Copy sources to temporary directory""$color_normal"

cp  -r ${in} ${tmp} && cd ${tmp}

check_status

echo -e "$color_success""Install dependencies""$color_normal"

yarn

check_status

echo -e "$color_success""Building bundle with command: \"${COMMAND}\"""$color_normal"

${COMMAND}

check_status

echo -e "$color_success""Bundle displacement to \"${in}\" dir""$color_normal"

cp -r ${tmp}/${BUILD_DIR}/* ${out}

check_status
